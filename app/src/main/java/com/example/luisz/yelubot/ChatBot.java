package com.example.luisz.yelubot;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luisz.yelubot.FragmentAbsMain;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

//import configuraciones de api ai;
import junit.framework.Test;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.AIListener;
import ai.api.android.AIService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatBot extends FragmentAbsMain implements AIListener{

    View view;
    RecyclerView recyclerView;
    EditText editText;
    RelativeLayout addBtn;

    private String UserId;
    // referencia de la base de datos.
    DatabaseReference ref;
    FirebaseRecyclerAdapter<ChatMessage,ChatRec> adapter;
    Boolean flagFab = true;
    private AIService aiService;


    // Cambiar texto a voz
    private TextToSpeech mTextToSpeech;
    public ChatBot() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_chat_bot, container, false);
        editText = (EditText)view.findViewById(R.id.editText);
        addBtn = (RelativeLayout)view.findViewById(R.id.addBtn );
        TestGoogleAct activity = (TestGoogleAct)getActivity();
        UserId = activity.getUserId();
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        // inicializa la referencia de la base de datos sincronizada con el proyecto.
        ref = FirebaseDatabase.getInstance().getReference();
        ref.keepSynced(true);
        // permisos para la grabacion de audio.
        ActivityCompat.requestPermissions(getActivity() ,new String[]{Manifest.permission.RECORD_AUDIO},1);
        // Configuracion de la IA
        final AIConfiguration aiConfiguration = new AIConfiguration("e8b070a4eb7c4e478273c9151796ed01",
                AIConfiguration.SupportedLanguages.Spanish,
                AIConfiguration.RecognitionEngine.System);
        // inicio del servicio AI.
        aiService = AIService.getService(getContext(),aiConfiguration);
        // escucha.
        aiService.setListener(this);
        // servicio de datos AI.
        final AIDataService aiDataService = new AIDataService(aiConfiguration);
        // evento del boton.
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = editText.getText().toString().trim();
                final AIRequest aiRequest = new AIRequest();
                // si el mensaje contiente un texto.
                if(!msg.equals("")){
                    ChatMessage cm = new ChatMessage(msg,"user");
                    ref.child("Usuario").child(UserId).child("chat").push().setValue(cm);
                    // envia una peticion con el mensaje y maneja la respuesta.
                    aiRequest.setQuery(msg);
                    new AsyncTask<AIRequest,Void,AIResponse>(){

                        @Override
                        protected AIResponse doInBackground(AIRequest... aiRequests) {
                            final AIRequest request = aiRequests[0];
                            try {
                                final AIResponse response = aiDataService.request(aiRequest);
                                return response;
                            } catch (AIServiceException e) {
                            }
                            return null;
                        }
                        @Override
                        protected void onPostExecute(AIResponse response) {
                            if (response != null) {

                                Result result = response.getResult();
                                String reply = result.getFulfillment().getSpeech();
                                ChatMessage chatMessage = new ChatMessage(reply, "bot");
                                ref.child("Usuario").child(UserId).child("chat").push().setValue(chatMessage);
                            }
                        }
                    }.execute(aiRequest);
                }else {
                    // si el campo de mensaje esta vacio comienza la grabaion de voz.
                    aiService.startListening();
                }
                // limpia el mensaje.
                editText.setText("");
            }
        });

        mTextToSpeech = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {

            }
        });
        recycleMsg();
        textChangedListener();

        return view;
    }


    // metodo para reciclar el layout de mensajes.
    public void recycleMsg(){
        // adaptador para iniciarlizar la lista de mensajes y obtener la referencia de la base de datos.
        adapter = new FirebaseRecyclerAdapter<ChatMessage, ChatRec>(ChatMessage.class,R.layout.msglist,ChatRec.class,ref.child("Usuario").child(UserId).child("chat")) {
            // manejo de mensajes usuario derecha bot izquierda.
            @Override
            protected void populateViewHolder(ChatRec viewHolder, ChatMessage model, int position) {
                if(model.getMsgUser().equals("user")){
                    viewHolder.rightText.setText(model.getMsgText());
                    viewHolder.rightText.setVisibility(View.VISIBLE);
                    viewHolder.leftText.setVisibility(View.GONE);
                }else{
                    viewHolder.leftText.setText(model.getMsgText());
                    viewHolder.leftText.setVisibility(View.VISIBLE);
                    viewHolder.rightText.setVisibility(View.GONE);
                }
            }
        };

        // obseervador para cambios en la base de datos.
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            // deteccion de un nuemvo item en la db, detecata la posicion en la que se va a insertar el mensaje en el marco.
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);

                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                linearLayoutManager.setStackFromEnd(true);
                recyclerView.setLayoutManager(linearLayoutManager);

                int msgCount = adapter.getItemCount();
                int lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                // baja hasta el ultimo mensaje registrado en la db.
                if(lastVisiblePosition == -1 ||
                    (positionStart >= (msgCount -1) &&
                            lastVisiblePosition == (positionStart - 1))){
                    recyclerView.scrollToPosition(positionStart);
                }
            }
        });
        // establece el adaptador a la vista.
        recyclerView.setAdapter(adapter);
    }

    // evento al escribir un mensaje.
    public void textChangedListener(){
        this.editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ImageView fab_img = (ImageView) view.findViewById(R.id.fab_img);
                Bitmap img = BitmapFactory.decodeResource(getResources(),R.drawable.ic_send_white_24dp);
                Bitmap img1 = BitmapFactory.decodeResource(getResources(),R.drawable.ic_mic_white_24dp);
                if (s.toString().trim().length()!=0 && flagFab){
                    ImageViewAnimatedChange(getActivity(),fab_img,img);
                    flagFab=false;
                }
                else if (s.toString().trim().length()==0){
                    ImageViewAnimatedChange(getActivity(),fab_img,img1);
                    flagFab=true;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    // animacion de cambio de icono.
    public void ImageViewAnimatedChange(Context c, final ImageView v, final Bitmap new_image) {
        final Animation anim_out = AnimationUtils.loadAnimation(c, R.anim.zoom_out);
        final Animation anim_in  = AnimationUtils.loadAnimation(c, R.anim.zoom_in);
        anim_out.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setImageBitmap(new_image);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(anim_in);
            }
        });
        v.startAnimation(anim_out);
    }

    // cunado se obtiene un resultado pone los mensajes en la db
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onResult(ai.api.model.AIResponse response) {


        Result result = response.getResult();

        String message = result.getResolvedQuery();
        ChatMessage chatMessage0 = new ChatMessage(message, "user");
        ref.child("Usuario").child(UserId).child("chat").push().setValue(chatMessage0);


        String reply = result.getFulfillment().getSpeech();
        mTextToSpeech.speak(reply, TextToSpeech.QUEUE_FLUSH, null, null);
        ChatMessage chatMessage = new ChatMessage(reply, "bot");
        ref.child("Usuario").child(UserId).child("chat").push().setValue(chatMessage);
        if (result.getAction().equals("alarma") && result.getParameters().get("time") != null) {
            String alarma = result.getParameters().get("time").toString();
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                Date date = sdf.parse(alarma.replaceAll("\"", ""));

//Podemos asignar la hora a una fecha
                Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
                calendar.setTime(date);   // assigns calendar to given date
//Podemos recuperar la hora, minuto, etc. de la fecha 
                int hour = calendar.get(Calendar.HOUR);
                establecerAlarma("Esto es una alarma de YeLuBot", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    public void onError(ai.api.model.AIError error) {

    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {

    }

    @Override
    public void onListeningCanceled() {

    }

    @Override
    public void onListeningFinished() {

    }

    public void establecerAlarma(String mensaje, int hora, int minuto) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, mensaje)
                .putExtra(AlarmClock.EXTRA_HOUR, hora)
                .putExtra(AlarmClock.EXTRA_MINUTES, minuto);

        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
