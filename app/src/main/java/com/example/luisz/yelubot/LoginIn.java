package com.example.luisz.yelubot;


import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import junit.framework.Test;

import java.util.concurrent.Executor;

import dmax.dialog.SpotsDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginIn extends FragmentAbsMain implements View.OnClickListener, TextWatcher {
    private TextInputLayout email;
    private TextInputLayout pass;
    private Button btnLogIn;
    private Button btnSignUp;

    private SignInButton btnSignInGoogle;
    private GoogleApiClient googleApiClient;

    private SpotsDialog spDialog;
    private AlertDialog alert;
    Usuario user;

    public static final int SIGN_IN_CODE = 777;


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    public LoginIn() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_in, container, false);
        email = (TextInputLayout) view.findViewById(R.id.InputLEmail);
        pass = (TextInputLayout) view.findViewById(R.id.InputLPass);

        email.getEditText().addTextChangedListener(this);
        pass.getEditText().addTextChangedListener(this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity(), new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getActivity().getApplicationContext(), "Problemas al logearse con Google", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        btnLogIn = (Button) view.findViewById(R.id.BtnLogIn);
        btnSignUp = (Button) view.findViewById(R.id.BtnSignUp);
        btnSignInGoogle = (SignInButton) view.findViewById(R.id.BtnLogInGoogle);
        btnSignInGoogle.setSize(SignInButton.SIZE_WIDE);
        btnSignInGoogle.setColorScheme(SignInButton.COLOR_DARK);

        alert = new SpotsDialog.Builder()
                .setContext(view.getContext())
                .setMessage("Ingresando...")
                .setCancelable(false)
                .build();
        // Hacemos los botones sensibles al click.
        btnLogIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        btnSignInGoogle.setOnClickListener(this);
        inicializarFirebase();
        return view;
    }

    /** Metodo para inicializar Firedatabase**/
    private void inicializarFirebase() {
        FirebaseApp.initializeApp(getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    public void registroNormal() {
        String username = email.getEditText().getText().toString();
        final String password = pass.getEditText().getText().toString();

        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(getActivity().getApplicationContext(), "Completa todos los campos!", Toast.LENGTH_SHORT).show();
        } else {
            alert.show();
            DatabaseReference usernamesRef = databaseReference.child("usernames").child(username);
            usernamesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        /** Aca me devuelve si el usuario ha sido encontrado **/
                        Toast.makeText(getActivity().getApplicationContext(),dataSnapshot.getValue().toString(), Toast.LENGTH_SHORT).show();
                        comprobarPassword(dataSnapshot.getValue().toString(), password);
                    } else {
                        /** Mostramos un mensaje de error en el EditText**/
                        email.setError("Usuario no encontrado.");
                        alert.dismiss();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    alert.dismiss();
                    Toast.makeText(getActivity().getApplicationContext(), "Fallo!"+databaseError, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void comprobarPassword(final String userId, final String passForCheck) {
        DatabaseReference usuarioRef = databaseReference.child("Usuario").child(userId);
        usuarioRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    /** Aca nos devuelve el Usuario y sus datos completos! **/
                    String passDb = dataSnapshot.child("password").getValue(String.class);
                    if (passForCheck.equals(passDb)) {
                        /** Darle paso a la pagina del chatbot **/
                        Usuario user = new Usuario();
                        user.setUserId(userId);
                        user.setPassword(passDb);
                        user.setEmail(dataSnapshot.child("email").getValue(String.class));
                        user.setPhotoUrl(dataSnapshot.child("photoUrl").getValue(String.class));
                        user.setUsername(dataSnapshot.child("username").getValue(String.class));
                        saveUser(user);
                        alert.dismiss();
                        Intent intent = new Intent(getActivity(), TestGoogleAct.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        pass.setError("Contraseña incorrecta.");
                    }
                } else {
                    /** Mostramos un mensaje de error en el EditText**/
                    email.setError("Usuario no encontrado.");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity().getApplicationContext(), "Fallo2!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.BtnLogIn:
                registroNormal();
                break;
            case R.id.BtnSignUp:

                ((FragmentActivityAbsMain) getActivity()).addFragmentToPila(new SignUp());
                break;

            case R.id.BtnLogInGoogle:

                Intent intent2 = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent2, SIGN_IN_CODE);
                alert.show();
                break;
        }

    }



    /** Metodo para guardar los datos **/
    public void saveUser(Usuario user) {
        SharedPreferences.Editor editor = ((FragmentActivityAbsMain)getActivity()).getEditor();
        editor.putString("Username", user.getUsername());
        editor.putString("Email", user.getEmail());
        editor.putString("PhotoUrl", user.getPhotoUrl());
        editor.putString("UserId", user.getUserId());
        editor.putString("Password", user.getPassword());
        editor.commit();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGN_IN_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {

            /** Debemos agregar al usuario al Fire Database si el mae no existe **/

            GoogleSignInAccount account = result.getSignInAccount();
            user = new Usuario();
            user.setUserId(account.getId());
            user.setUsername(account.getDisplayName());
            databaseReference.child("Usuario").child(user.getUserId()).setValue(user, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference dRef) {
                    if (databaseError != null) {

                    } else {
                        //Toast.makeText(getActivity().getApplicationContext(), "Registrado correctamente.", Toast.LENGTH_SHORT).show();
                        databaseReference.child("usernames").child(user.getUsername()).setValue(user.getUserId());
                        alert.dismiss();
                    }
                }
            });
            //((FragmentActivityAbsMain) getActivity()).addFragmentToPila(new UserMainView());
            Intent intent = new Intent(this.getActivity(), TestGoogleAct.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            alert.dismiss();
            Toast.makeText(getActivity().getApplicationContext(), "Problemas al logearse con Google", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable == email.getEditText().getEditableText()) {
            /** Estoy escribiendo en mi username**/
            if (email.getEditText().getText().toString().isEmpty()) {
                email.setError("Completa este campo.");
            } else {
                email.setError(null);
            }
        } else if (editable == pass.getEditText().getEditableText()) {
            /** Estoy escribiendo en mi password**/
            if (pass.getEditText().getText().toString().isEmpty()) {
                pass.setError("Completa este campo.");
            } else {
                pass.setError(null);
            }
        }
    }
}
