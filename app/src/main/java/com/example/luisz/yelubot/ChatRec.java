package com.example.luisz.yelubot;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by User on 30/04/2019.
 */

public class ChatRec extends RecyclerView.ViewHolder{
    TextView leftText,rightText;

    public ChatRec(View itemView){
        super(itemView);

        leftText = (TextView)itemView.findViewById(R.id.leftText);
        rightText = (TextView)itemView.findViewById(R.id.rightText);

    }
}
