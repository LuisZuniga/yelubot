package com.example.luisz.yelubot;


import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.net.VpnService;
import android.os.Bundle;
import android.os.PatternMatcher;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;
import java.util.regex.Pattern;

import dmax.dialog.SpotsDialog;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUp extends FragmentAbsMain implements View.OnClickListener, TextWatcher {

    /** declaramos nuestros atributos o objetos de la vista */
    View view;
    private Button BtnSignUp;
    private Button BtnSaveUserPhoto;
    private TextInputLayout Email;
    private TextInputLayout Username;
    private TextInputLayout Password;
    private TextInputLayout VPassword;
    private ImageView imgUser;

    private Uri uriUserPath;
    Usuario user;

    private SpotsDialog spDialog;
    private AlertDialog alert;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseStorage storage;
    StorageReference storageRef;
    private EditText EditTEmail;
    public SignUp() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        user = new Usuario();
        /** Ligamos nuestras variable con la vista **/
        Email = (TextInputLayout) view.findViewById(R.id.ILEmail);
        Username = (TextInputLayout) view.findViewById(R.id.ILUsername);
        Password = (TextInputLayout) view.findViewById(R.id.ILPassword);
        VPassword = (TextInputLayout) view.findViewById(R.id.ILVPassword);

        EditTEmail = view.findViewById(R.id.TxtEmail);
        EditTEmail.addTextChangedListener(this);
        //Email.getEditText().addTextChangedListener(this);
        Username.getEditText().addTextChangedListener(this);
        Password.getEditText().addTextChangedListener(this);
        VPassword.getEditText().addTextChangedListener(this);

        BtnSignUp = (Button) view.findViewById(R.id.BtnSignUpSave);
        BtnSignUp.setEnabled(false);
        BtnSaveUserPhoto = (Button) view.findViewById(R.id.btnSavePhoto);

        imgUser = (ImageView) view.findViewById(R.id.userPhoto);

        BtnSignUp.setOnClickListener(this);
        BtnSaveUserPhoto.setOnClickListener(this);

        alert = new SpotsDialog.Builder()
                .setContext(view.getContext())
                .setMessage("Registrando usuario...")
                .setCancelable(false)
                .build();

        inicializarFirebase();
        inicializarFirebaseStorage();
        return view;
    }


    /** Metodo para inicializar Firedatabase**/
    private void inicializarFirebase() {
        FirebaseApp.initializeApp(getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    private void inicializarFirebaseStorage() {
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://yelubot.appspot.com/");
        //gs://yelubot.appspot.com/UsersPhotos
    }

    /** Esto metodo sera para registrar un usuario en el sistema**/
    public void registrarUsuario () {
        String email = Email.getEditText().getText().toString();
        String username = Username.getEditText().getText().toString();
        String pass = Password.getEditText().getText().toString();
        String vPass = VPassword.getEditText().getText().toString();

        /** Verificamos que todos los campos esten con datos != vacio**/
        if (email.isEmpty()) {
            Email.setError("Por favor completa este campo.");
        } else if (username.isEmpty()) {
            Username.setError("Por favor completa este campo.");
        } else if (pass.isEmpty()) {
            Password.setError("Por favor completa este campo.");
        } else if (vPass.isEmpty()) {
            VPassword.setError("Por favor completa este campo.");
        } else {
            alert.show();
            user.setUserId(UUID.randomUUID().toString());
            user.setUsername(username);
            user.setEmail(email);
            user.setPassword(pass);

            StorageReference usersPhotosRef = storageRef.child("UserPhotos/"+uriUserPath.getLastPathSegment());
            UploadTask uptask = usersPhotosRef.putFile(uriUserPath);
            // En caso de que falle la subida
            uptask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity().getApplicationContext(), "Error al subir imagen."+e, Toast.LENGTH_SHORT).show();
                }
            });

            // Exito
            uptask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    /* Si subimos bien la imagen, ahora hacemos el registro.. */
                    user.setPhotoUrl(taskSnapshot.getDownloadUrl().toString());
                    Toast.makeText(getActivity().getApplicationContext(), "Subida correctamente!", Toast.LENGTH_SHORT).show();
                    databaseReference.child("Usuario").child(user.getUserId()).setValue(user, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference dRef) {
                            if (databaseError != null) {
                                /** Debemos eliminar la imagen que subidos anteriormente. **/
                                Toast.makeText(getActivity().getApplicationContext(), "No se pudo registrar. " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(getActivity().getApplicationContext(), "Registrado correctamente.", Toast.LENGTH_SHORT).show();
                                databaseReference.child("usernames").child(user.getUsername()).setValue(user.getUserId());
                                alert.dismiss();
                                limpiarCampos();
                            }
                        }
                    });
                }
            });
        }
    }

    public boolean validarRegistro(Editable editable) {
        boolean flag = true;
        if (editable == Username.getEditText().getEditableText()) {
            if (Username.getEditText().getText().toString().isEmpty()) {
                Username.setError("Campo requerido.");
                flag = false;
            } else {
                Username.setError(null);
            }

        } else if (editable == Email.getEditText().getEditableText()) {
            if (validarEmail(Email.getEditText().getText().toString())) {
                Email.setError(null);
            } else {
                Email.setError("El email no es correcto.");
                flag = false;
            }
        } else if (editable == Password.getEditText().getEditableText() ) {
            if (validarPass(Password.getEditText().getText().toString())) {
                Password.setError(null);
            } else {
                Password.setError("La contraseña no coincide con los caracteres necesarios.");
                flag = false;
            }
        } else if (editable == VPassword.getEditText().getEditableText()) {
            if (verificarPassword(Password.getEditText().getText().toString(), VPassword.getEditText().getText().toString())) {
                VPassword.setError(null);
            } else {
                VPassword.setError("Las contraseñas no coinciden.");
                flag = false;
            }
        }
        administrarBtnSignUp(flag);
        return flag;
    }

    public void administrarBtnSignUp(boolean flag) {
        BtnSignUp.setEnabled(flag);
    }

    /** Validar Email **/

    public boolean validarEmail(String email) {
        Pattern patteer = Patterns.EMAIL_ADDRESS;
        return patteer.matcher(email).matches();
    }

    /** Valida contrasena **/

    public boolean validarPass(String pass) {
        Pattern regex =  Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
        return regex.matcher(pass).matches();
    }

    public boolean verificarPassword (String pass, String pass2) {
        return pass.equals(pass2);
    }

    public void limpiarCampos() {
        Email.getEditText().setText("");
        Username.getEditText().setText("");
        Password.getEditText().setText("");
        VPassword.getEditText().setText("");
        imgUser.setImageResource(R.mipmap.ic_user);
    }

    public void cargarImagenUsuario() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent, "Seleccione la aplicación"), 10);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            uriUserPath = data.getData();
            imgUser.setImageURI(uriUserPath);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.BtnSignUpSave:
                registrarUsuario();
                break;

            case R.id.btnSavePhoto:
                cargarImagenUsuario();
                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void afterTextChanged(Editable editable) {
        validarRegistro(editable);
    }
}
