package com.example.luisz.yelubot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.w3c.dom.Text;

import dmax.dialog.SpotsDialog;

public class TestGoogleAct extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

        private TextView UserName;
        private TextView UserEmail;
        private TextView UserId;
        private ImageView UserPhoto;
        private Usuario firebaseUser;

        GoogleApiClient googleApiClient;
        private AlertDialog logOutAlert;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_test_google);
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                toolbar.setTitle("YeLuBot");
                setSupportActionBar(toolbar);

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                drawer.addDrawerListener(toggle);
                toggle.syncState();

                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setNavigationItemSelectedListener(this);

                /**Dialog que aparece cuando vamos a cerrar la session**/
                logOutAlert = new SpotsDialog.Builder()
                        .setContext(this)
                        .setMessage("Cerrando sesión...")
                        .setCancelable(false)
                        .build();

                firebaseUser = getUser();
                /** PASOS PARA OBTENER DATOS DEL USUARIO CONECTADO**/
                View headerView = navigationView.getHeaderView(0);
                UserName = (TextView) headerView.findViewById(R.id.TvUser);
                UserEmail = (TextView) headerView.findViewById(R.id.TvUserEmail);
                UserId = (TextView) headerView.findViewById(R.id.TvUserId);
                UserPhoto = (ImageView) headerView.findViewById(R.id.IVUserPhoto);

                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();

                googleApiClient = new GoogleApiClient.Builder(this)
                        .enableAutoManage(this, this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
        }

        @Override
        public void onBackPressed() {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                } else {
                        super.onBackPressed();
                }
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.main, menu);
                return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
                // Handle action bar item clicks here. The action bar will
                // automatically handle clicks on the Home/Up button, so long
                // as you specify a parent activity in AndroidManifest.xml.
                int id = item.getItemId();

                return super.onOptionsItemSelected(item);
        }

        public void establecerAlarma(String mensaje, int hora, int minuto) {
                Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                        .putExtra(AlarmClock.EXTRA_MESSAGE, mensaje)
                        .putExtra(AlarmClock.EXTRA_HOUR, hora)
                        .putExtra(AlarmClock.EXTRA_MINUTES, minuto);

                if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                }
        }

        public String getUserId() {
                return UserId.getText().toString();
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
                // Handle navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.nav_camera) {
                        // Llamar un nuevo fragmento.


                } else if (id == R.id.nav_gallery) {
                        establecerAlarma("Soy una alarma de prueba", 14, 10);
                } else if (id == R.id.nav_slideshow) {

                } else if (id == R.id.nav_manage) {

                } else if (id == R.id.nav_logout) {
                        /** Este se va a ejecutar cuando querramos cambiar de cuenta */
                        logOutAlert.show();
                        revokeSession(this.getWindow().getDecorView().findViewById(android.R.id.content));
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
        }

        /** Cerrar session**/
        public void logOut(View view) {
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                                goLogInScreen();
                        } else {
                                //Toast.makeText(getApplicationContext(), "No se pudo cerrar sesión.", Toast.LENGTH_SHORT).show();
                                cerrarFirebaseUserSesion();
                        }
                        }
                });
        }

        /** Cambiar cuenta **/
        /** Eliminar todo tipo de coneccion con google**/
        public void revokeSession (View view) {
                Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                                goLogInScreen();
                        } else {
                                //Toast.makeText(getApplicationContext(), "No se pudo revocar la cuenta", Toast.LENGTH_SHORT).show();
                                cerrarFirebaseUserSesion();
                        }
                        }
                });
        }

        /** Login silencioso **/

        @Override
        protected void onStart() {
                super.onStart();
                OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
                if (opr.isDone()) {
                        GoogleSignInResult result = opr.get();
                        handleSignResult(result);
                } else {
                        opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                                @Override
                                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                                        handleSignResult(googleSignInResult);
                                }
                        });
                }
        }

        private void handleSignResult(GoogleSignInResult result) {
                if (result.isSuccess()) { // Si me pude logear con Google!
                        GoogleSignInAccount account = result.getSignInAccount();
                        Toast.makeText(this, "Hola "+account.getDisplayName(), Toast.LENGTH_SHORT).show();
                        UserName.setText(account.getDisplayName());
                        UserEmail.setText(account.getEmail());
                        UserId.setText(account.getId());
                        Glide.with(this).load(account.getPhotoUrl()).into(UserPhoto);
                        New_Fragment(new ChatBot());
                } else if (!firebaseUser.getUsername().isEmpty()){
                        Toast.makeText(this, "Hola "+firebaseUser.getUsername(), Toast.LENGTH_SHORT).show();
                        UserName.setText(firebaseUser.getUsername());
                        UserEmail.setText(firebaseUser.getEmail());
                        UserId.setText(firebaseUser.getUserId());
                        Glide.with(this).load(firebaseUser.getPhotoUrl()).into(UserPhoto);
                        New_Fragment(new ChatBot());
                } else { // No tengo ningun LogIn Activo
                        goLogInScreen();
                }
        }

        private void goLogInScreen() {
                logOutAlert.dismiss();
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
        }

        public Usuario getUser() {
                Usuario user = new Usuario();
                SharedPreferences preferences = getSharedPreferences("Usuario",0);
                user.setUserId(preferences.getString("UserId",""));
                user.setUsername(preferences.getString("Username", ""));
                user.setEmail(preferences.getString("Email", ""));
                user.setPhotoUrl(preferences.getString("PhotoUrl", ""));
                user.setPassword(preferences.getString("Password", ""));
                return user;
        }

        public void cerrarFirebaseUserSesion() {
                SharedPreferences sp = getSharedPreferences("Usuario", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.remove("Username");
                editor.remove("Email");
                editor.remove("PhotoUrl");
                editor.remove("UserId");
                editor.remove("Password");
                editor.clear();
                if (editor.commit()){
                        Toast.makeText(this, "Eliminado!", Toast.LENGTH_SHORT).show();
                }
                goLogInScreen();
        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        }

        /** Tuve que declarar de nuevo este metodo para esta clase porque no podia hacer el
         * llamado de los metodos de la clase abstracta.**/
        public void New_Fragment(FragmentAbsMain fragment){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.BackgroundContainer , fragment).commit();

        }

        public void addFragmentToPila(FragmentAbsMain fragment){
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.BackgroundContainer, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.addToBackStack(null);
                ft.commitAllowingStateLoss();
        }
}
