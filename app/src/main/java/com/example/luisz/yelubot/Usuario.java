package com.example.luisz.yelubot;

/**
 * Created by Luisz on 1/6/2019.
 */

public class Usuario {
    private String userId;
    private String username;
    private String email;
    private String password;
    private String photoUrl;

    public Usuario() {
    }

    public Usuario(String userId, String username, String email, String password, String photoUrl) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.password = password;
        this.photoUrl = photoUrl;
    }

    public Usuario(String username, String userId) {
        this.username = username;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

}
