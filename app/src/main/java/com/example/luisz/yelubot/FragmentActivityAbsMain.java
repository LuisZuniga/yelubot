package com.example.luisz.yelubot;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by Luisz on 26/4/2019.
 */

public abstract class FragmentActivityAbsMain extends AppCompatActivity {

    // Bandera para poder cambiar el fragmento.
    protected boolean change_fragment = true;
    public String File_Name_User = "Usuario";

    // me retorna el valor de la bandera que permite cambiar de fragmento.
    public boolean if_can_change_fragment(){
        return this.change_fragment;
    }

    public void New_Fragment(FragmentAbsMain fragment){
        (getSupportFragmentManager().beginTransaction()
                .replace(R.id.Container , fragment))
                .commit();
    }

    public void addFragmentToPila(FragmentAbsMain fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.Container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if (this.if_can_change_fragment()) {
            //Toast.makeText(this, "Testing this", Toast.LENGTH_SHORT).show();
            //            super.onBackPressed(); // Funcionalidad para el boton de back del telefono
        }
    }


    protected SharedPreferences.Editor getEditor(){
        SharedPreferences sp = getSharedPreferences(File_Name_User, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        return editor;
    }

}
